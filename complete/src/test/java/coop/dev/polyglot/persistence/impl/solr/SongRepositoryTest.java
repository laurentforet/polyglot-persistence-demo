package coop.dev.polyglot.persistence.impl.solr;

import coop.dev.polyglot.persistence.domain.Identifiable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 *
 * @author lfo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {SolrConfig.class})
public class SongRepositoryTest {
    
    
    @Autowired
    private SongRepository songRepository;
    
    @Test
    public void testSimple() {
//        SongCollection song = new SongCollection(Identifiable.Generator.INSTANCE.generate(), "title","lyrics");
//        songRepository.save(song);
    }
}
