package coop.dev.polyglot.persistence.impl.mongodb;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author lfo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {MongoConfig.class})
public class CatalogInitializerTest {

    @Autowired
    private CatalogInitializer catalogInitializer;
    @Autowired
    private SongRepository songRepository;
    
    @Test
    public void testInit() {
        songRepository.deleteAll();
        catalogInitializer.init();
        Assert.assertNotNull(songRepository.findByTitle("Tom's Diner"));
    }
}
