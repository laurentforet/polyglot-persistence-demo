package coop.dev.polyglot.persistence.impl.mongodb;

import coop.dev.polyglot.persistence.impl.Const;
import java.util.Collections;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {MongoConfig.class})
public class SongRepositoryTest {
    
    private static final String THE__SOLDIER_AND__THE__QUEEN_ID = Const.THE_SOLDIER_AND_THE_QUEEN_ID;
    private static final String THE__SOLDIER_AND__THE__QUEEN = Const.THE_SOLDIER_AND_THE_QUEEN;
    private static final String SUZANE_VEGA_ID = Const.SUZANE_VEGA_ID;
    private static final String SUZANE__VEGA = Const.SUZANE_VEGA;
   

    @Autowired
    private SongRepository songRepository;

    @Test
    public void minimalTest() {
        songRepository.deleteAll();
        final SongDocument.ArtistMongo suzaneVega = new SongDocument.ArtistMongo(SUZANE_VEGA_ID, SUZANE__VEGA);
        songRepository.save(new SongDocument(
                THE__SOLDIER_AND__THE__QUEEN_ID, 
                THE__SOLDIER_AND__THE__QUEEN,
                "The soldier came knocking upon the Queen's door ...", suzaneVega));
        Assert.assertEquals(1, songRepository.findAll().size());
        final SongDocument soldierFoundByTitle = songRepository.findByTitle(THE__SOLDIER_AND__THE__QUEEN);
        Assert.assertNotNull(soldierFoundByTitle);
        Assert.assertEquals(THE__SOLDIER_AND__THE__QUEEN_ID, soldierFoundByTitle.getUuid());
        Assert.assertEquals(1, songRepository.findByArtist(suzaneVega).size());
        Assert.assertEquals(1, songRepository.findByArtist(SUZANE_VEGA_ID).size());
        Assert.assertEquals(1, songRepository.findByUuidIn(Collections.singleton(THE__SOLDIER_AND__THE__QUEEN_ID)).size());
        
        
    }
}
