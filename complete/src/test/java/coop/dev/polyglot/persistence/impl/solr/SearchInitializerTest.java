package coop.dev.polyglot.persistence.impl.solr;

import java.util.Arrays;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 *
 * @author lfo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {SolrConfig.class})
public class SearchInitializerTest {

    @Autowired
    private SearchInitializer searchInitializer;
    @Autowired
    private SongRepository songRepository;
    
    @Test
    public void testInit() {
//        songRepository.deleteAll();
//        searchInitializer.init();
//        Assert.assertEquals(1, songRepository.findByLyricsContaining(Arrays.asList("Tom"), new PageRequest(1, 10)).getContent().size()) ;
    }
}
