package coop.dev.polyglot.persistence.impl.redis;
import coop.dev.polyglot.persistence.service.BasketService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author lforet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {RedisConfig.class})
public class BasketServiceRedisTest {

    @Autowired
    private BasketService basketService;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    
    @Test
    public void testAdd() {
        basketService.clear("lforet");
        
        basketService.add("lforet", "1234");
        Assert.assertEquals(1, basketService.getAll("lforet").size());
        basketService.add("lforet", "1234");
        Assert.assertEquals(1, basketService.getAll("lforet").size());
        basketService.add("lforet", "12345");
        Assert.assertEquals(2, basketService.getAll("lforet").size());
        basketService.remove("lforet", "1234");
    }
    
//    @After
//    public void tearDown() throws Exception {
//        redisTemplate.getConnectionFactory().getConnection().flushDb();
//        redisTemplate.getConnectionFactory().getConnection().close();
//    }
   
}
