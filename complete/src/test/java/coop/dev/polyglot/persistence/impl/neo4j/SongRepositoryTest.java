package coop.dev.polyglot.persistence.impl.neo4j;

import coop.dev.polyglot.persistence.domain.Identifiable;
import coop.dev.polyglot.persistence.impl.Const;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 *
 * @author lforet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {Neo4jConfig.class})
public class SongRepositoryTest {

    @Autowired
    private GraphDatabaseService graphDatabaseService;

    @Autowired
    private SongRepository songRepository;

    @Test
    public void testMinimal() {
        try (Transaction tx = graphDatabaseService.beginTx()) {
            songRepository.deleteAll();
            songRepository.save(new SongNode(Const.THE_SOLDIER_AND_THE_QUEEN_ID, Const.THE_SOLDIER_AND_THE_QUEEN));
        
            final SongNode songFound = songRepository.findByTitle(Const.THE_SOLDIER_AND_THE_QUEEN).iterator().next();
            Assert.assertEquals(Const.THE_SOLDIER_AND_THE_QUEEN_ID, songFound.getUuid());
        }
    }
}
