package coop.dev.polyglot.persistence.impl.neo4j;

import java.util.Iterator;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.GraphDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 *
 * @author lfo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {Neo4jConfig.class})
public class RecommendationInitializerTest {

    @Autowired
    private GraphDatabaseService graphDatabaseService;
    @Autowired
    private RecommendationInitializer initializer;

    @Autowired
    private SongRepository songRepository;
   
    @Autowired 
    private PersonRepository personRepository;

    @Test
    public void test() {
        initializer.init();
        final Iterable<PersonNode> found = personRepository.findByLogin("Benoit P.");
        PersonNode benoit = found.iterator().next();
        final Iterator<SongNode> recommendedSongs = songRepository.findRecommendedSongs(benoit).iterator();
        Assert.assertEquals("Sober", recommendedSongs.next().getTitle());
        Assert.assertEquals("Mother", recommendedSongs.next().getTitle());
        Assert.assertEquals("Soul Catcher", recommendedSongs.next().getTitle());
    }

}
