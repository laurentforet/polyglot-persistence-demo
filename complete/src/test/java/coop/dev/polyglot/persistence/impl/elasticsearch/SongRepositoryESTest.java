package coop.dev.polyglot.persistence.impl.elasticsearch;

import coop.dev.polyglot.persistence.impl.Const;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author lfo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {ElasticsearchConfig.class})
public class SongRepositoryESTest {
    
    @Autowired 
    private SongRepositoryES songRepository;
    
    @Test
    public void testSimple() {
        songRepository.deleteAll();
        SongDocumentES song = new SongDocumentES(Const.THE_SOLDIER_AND_THE_QUEEN_ID, Const.THE_SOLDIER_AND_THE_QUEEN, "Lyrics");
        songRepository.save(song);
        Assert.assertNotNull(songRepository.findOne(Const.THE_SOLDIER_AND_THE_QUEEN_ID));
        Assert.assertNotNull(songRepository.findByTitle(Const.THE_SOLDIER_AND_THE_QUEEN));
        final List<SongDocumentES> searchedSongs = songRepository.findByLyricsContaining("yri");
        Assert.assertEquals(Const.THE_SOLDIER_AND_THE_QUEEN_ID, searchedSongs.get(0).getUuid());
    }
    
}
