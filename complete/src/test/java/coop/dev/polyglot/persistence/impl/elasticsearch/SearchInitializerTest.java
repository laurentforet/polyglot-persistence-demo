package coop.dev.polyglot.persistence.impl.elasticsearch;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author lfo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {ElasticsearchConfig.class})
public class SearchInitializerTest {

    @Autowired
    private SearchInitializer searchInitializer;
    @Autowired
    private SongRepositoryES songRepository;
    
    @Test
    public void testInit() {
        songRepository.deleteAll();
        searchInitializer.init();
        songRepository.findOne("b9ef142b-1624-4771-5f4749c13db1");
        Assert.assertEquals(1, songRepository.findByTitleContainingOrLyricsContaining("Moth", "Mother").size()) ;
        final SongDocumentES searchByKeyword = songRepository.findByKeyword("mother").get(0);
        Assert.assertNotNull(searchByKeyword);
                
    }
}
