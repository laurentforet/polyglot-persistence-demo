package coop.dev.polyglot.persistence.impl;

import coop.dev.polyglot.persistence.domain.Identifiable;

/**
 *
 * @author lfo
 */
public class Const {
    
    //Persons
    public static final String LAURENT_ID = Identifiable.Generator.INSTANCE.generate();
    public static final String LAURENT = "Laurent";
    
    // Artists
    public static final String SUZANE_VEGA_ID = Identifiable.Generator.INSTANCE.generate();
    public static final String SUZANE_VEGA = "Suzane Vega";

    
    // Songs
    public static final String THE_SOLDIER_AND_THE_QUEEN_ID = Identifiable.Generator.INSTANCE.generate();
    public static final String THE_SOLDIER_AND_THE_QUEEN = "The Soldier and The Queen";
}
