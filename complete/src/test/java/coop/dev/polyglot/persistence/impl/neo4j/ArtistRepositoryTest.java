package coop.dev.polyglot.persistence.impl.neo4j;

import coop.dev.polyglot.persistence.impl.Const;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 *
 * @author lforet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {Neo4jConfig.class})
public class ArtistRepositoryTest {
    
    @Autowired
    private GraphDatabaseService graphDatabaseService;
    
    @Autowired
    private ArtistRepository artistRepository;
    @Autowired
    private SongRepository songRepository;
    
    @Test
    public void testMinimal() {
        try (Transaction tx = graphDatabaseService.beginTx()) {
            final SongNode soldierSong = new SongNode(Const.THE_SOLDIER_AND_THE_QUEEN_ID, Const.THE_SOLDIER_AND_THE_QUEEN);
            songRepository.save(soldierSong);
            final ArtistNode suzaneVega = new ArtistNode(Const.SUZANE_VEGA_ID, Const.SUZANE_VEGA);
            suzaneVega.addRecorded(soldierSong);
            artistRepository.save(suzaneVega);
            
            final ArtistNode artistFound = artistRepository.findByName(Const.SUZANE_VEGA).iterator().next();
            Assert.assertEquals(artistFound.getRecordedSongs().iterator().next().getUuid(), soldierSong.getUuid());  // importance du Fetch dans la relation
        } 
    }
}
