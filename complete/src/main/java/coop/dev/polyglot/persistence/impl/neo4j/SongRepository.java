package coop.dev.polyglot.persistence.impl.neo4j;

import java.util.List;
import org.springframework.data.neo4j.annotation.MapResult;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.annotation.ResultColumn;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author lforet
 */
public interface SongRepository extends CrudRepository<SongNode, String> {

    Iterable<SongNode> findByTitle(String title);

    @Query("start person=node({0})\n"
            + "MATCH (person)-[:LOVES]->(song:_Song)<-[:LOVES]-(otherPerson:_Person)-[:LOVES]->(otherSong:_Song)<-[otherLoves:LOVES]-()\n"
            + "WITH otherSong as otherSong, count(otherLoves) AS counter\n"
            + "RETURN otherSong\n"
            + "ORDER BY counter DESC , otherSong.title\n"
            + "LIMIT 3\n"
//            + "UNION\n"
//            + "MATCH ()-[loves2:LOVES]->(song2:Song)\n"
//            + "WITH song2.title AS title, count(loves2) AS counter\n"
//            + "RETURN song2\n"
//            + "ORDER BY counter DESC , title\n"
//            + "LIMIT 3"
    )
    List<SongNode> findRecommendedSongs(PersonNode person);
    
//    @MapResult
//    public interface MovieRecommendation {
//        
//        @ResultColumn("OtherSong")
//        SongNode getSong();
//        
//        @ResultColumn("numberOfLoves")
//        int getNumberOfLoves();
//        
//    }

}
