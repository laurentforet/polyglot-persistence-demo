package coop.dev.polyglot.persistence.service;

import coop.dev.polyglot.persistence.domain.Artist;
import coop.dev.polyglot.persistence.domain.Song;
import java.util.List;

/**
 *
 * @author lforet
 */
public interface RecommendationService {
    
    List<Song> getRecommendedSongs(String userId);
    
}
