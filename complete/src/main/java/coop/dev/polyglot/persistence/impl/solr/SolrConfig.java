package coop.dev.polyglot.persistence.impl.solr;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

/**
 *
 * @author lfo
 */
@Configuration
@EnableSolrRepositories(basePackages = {"coop.dev.polyglot.persistence.impl.solr"}, multicoreSupport = true)
@ComponentScan(basePackageClasses = SolrConfig.class)
public class SolrConfig {

    @Bean
    public SolrServer solrServer(@Value("http://localhost:8983/solr") String solrHost) {
        return new HttpSolrServer(solrHost);
    }
}
