package coop.dev.polyglot.persistence.impl.neo4j;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author lforet
 */
public interface ArtistRepository extends CrudRepository<ArtistNode, String> {
    
    
    Iterable<ArtistNode> findByName(String name);
}
