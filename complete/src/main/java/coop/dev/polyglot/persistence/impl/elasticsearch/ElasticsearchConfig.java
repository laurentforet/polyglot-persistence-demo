package coop.dev.polyglot.persistence.impl.elasticsearch;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author lfo
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = ElasticsearchConfig.class)
public class ElasticsearchConfig {
    
   
}
