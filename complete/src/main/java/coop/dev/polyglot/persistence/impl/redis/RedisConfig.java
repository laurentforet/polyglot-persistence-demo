/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package coop.dev.polyglot.persistence.impl.redis;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author lforet
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class RedisConfig {
//
//    @Bean(name = "jedisConnectionFactory")
//    public RedisConnectionFactory getRedisConnectionFactory() {
//        return new JedisConnectionFactory();
//    }
//
//    @Bean(name = "redisTemplate")
//    public RedisTemplate getRedisTemplate() {
//        RedisTemplate toReturn = new RedisTemplate();
//        toReturn.setConnectionFactory(getRedisConnectionFactory());
//        return toReturn; 
//   }
    
}
