package coop.dev.polyglot.persistence.service;

import coop.dev.polyglot.persistence.domain.Song;
import java.util.List;

/**
 *
 * @author lforet
 */
public interface BuyService {
    
    double getPrice(Song song);
    
    void buy(List<Song> song);
    
    
}
