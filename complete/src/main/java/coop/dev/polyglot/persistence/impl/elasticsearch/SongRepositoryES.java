package coop.dev.polyglot.persistence.impl.elasticsearch;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 *
 * @author lfo
 */
public interface SongRepositoryES extends ElasticsearchRepository<SongDocumentES, String> {
    
    SongDocumentES findByTitle(String title);
    Page<SongDocumentES> findByLyricsContaining(String lyrics, Pageable pageable);
    List<SongDocumentES> findByLyricsContaining(String lyrics);
    List<SongDocumentES> findByTitleContainingOrLyricsContaining(String title, String lyrics);
    
    
    @Query("{\n" +
"        \"bool\": {\n" +
"            \"should\": [\n" +
"                {\n" +
"                    \"term\": {\n" +
"                        \"title\": {\n" +
"                            \"value\": \"?0\",\n" +
"                            \"boost\": 3\n" +
"                        }\n" +
"                    }\n" +
"                },\n" +
"                {\n" +
"                    \"term\": {\n" +
"                        \"lyrics\": \"?0\"\n" +
"                    }\n" +
"                }\n" +
"            ]\n" +
"        }\n" +
"    }"
)
    List<SongDocumentES> findByKeyword(String keyword);
}
