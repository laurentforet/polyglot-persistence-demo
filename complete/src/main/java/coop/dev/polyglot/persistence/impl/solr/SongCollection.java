package coop.dev.polyglot.persistence.impl.solr;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.dev.polyglot.persistence.domain.Song;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

/**
 *
 * @author lfo
 */
@SolrDocument(solrCoreName = "collection1")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SongCollection implements Song {

    @Id @Indexed
    @Field
    private String id;
    
//    @Field
//    private String uuid;

    @Field
    @Indexed(boost = 1.0f)
    private String title;
    @Field
    @Indexed(boost = 0.5f)
    private String lyrics;

    public SongCollection() {
    }

    public SongCollection(String id, String title, String lyrics) {
        this.id = id;
        this.title = title;
        this.lyrics = lyrics;
    }

    @Override
    public String getUuid() {
        return id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public void setUuid(String uuid) {
//        this.uuid = uuid;
//    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }
}
