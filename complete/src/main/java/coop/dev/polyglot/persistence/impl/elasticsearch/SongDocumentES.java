package coop.dev.polyglot.persistence.impl.elasticsearch;

import coop.dev.polyglot.persistence.domain.Song;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 *
 * @author lfo
 */
@Document(indexName = "songindex", type = "song")
public class SongDocumentES implements Song {
    
    @Id
    private String uuid;
    @Field(type = FieldType.String)
    private String title;
    
    @Field(type = FieldType.String)
    private String lyrics;

    public SongDocumentES() {
    }

    public SongDocumentES(String uuid, String title, String lyrics) {
        this.uuid = uuid;
        this.title = title;
        this.lyrics = lyrics;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }
    
}
