package coop.dev.polyglot.persistence.impl.solr;

import java.io.IOException;
import java.util.Arrays;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component 
public class SearchInitializer {
    
    @Autowired
    private SongRepository songRepository;
   
    
    public void init() {
        try {
            SongCollection[] songs = SONG_MAPPER.readValue(this.getClass().getClassLoader().getResourceAsStream(SONGS_JSON), SongCollection[].class);
            songRepository.save(Arrays.asList(songs));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        
    }
    
    private static final String SONGS_JSON = "songCollections.json";
    private static final ObjectMapper SONG_MAPPER = new ObjectMapper();
}
