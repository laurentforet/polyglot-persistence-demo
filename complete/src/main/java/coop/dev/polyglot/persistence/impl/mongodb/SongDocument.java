package coop.dev.polyglot.persistence.impl.mongodb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.dev.polyglot.persistence.domain.Song;
import java.io.Serializable;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.data.annotation.Id;

/**
 *
 * @author lforet
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SongDocument implements Song {
    
    @Id
    private String uuid;

    private String title;
    
    private String lyrics;
    

    @JsonSerialize
    @JsonDeserialize
    private ArtistMongo artist; 

    public SongDocument() {
    }

    public SongDocument(String uuid, String title, String lyrics, ArtistMongo artist) {
        this.uuid = uuid;
        this.lyrics = lyrics;
        this.title = title;
        this.artist = artist;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    @Override
    public String toString() {
        return "SongDocument{" + "uuid=" + uuid + ", title=" + title + ", lyrics=" + lyrics + ", artist=" + artist + '}';
    }

    
    public static class ArtistMongo implements coop.dev.polyglot.persistence.domain.Artist, Serializable {
        
        private String uuid;
        private String name;

        public ArtistMongo() {
        }

        public ArtistMongo(String uuid) {
            this.uuid = uuid;
        }

        public ArtistMongo(String uuid, String name) {
            this.uuid = uuid;
            this.name = name;
        }

        @Override
        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        @Override
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "ArtistMongo{" + "uuid=" + uuid + ", name=" + name + '}';
        }

    }
    
    
}
