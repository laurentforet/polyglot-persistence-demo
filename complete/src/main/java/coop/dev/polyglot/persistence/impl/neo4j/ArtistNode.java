package coop.dev.polyglot.persistence.impl.neo4j;

import coop.dev.polyglot.persistence.domain.Artist;
import java.util.HashSet;
import java.util.Set;
import org.neo4j.graphdb.Direction;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

/**
 *
 * @author lfo
 */
@NodeEntity
@TypeAlias("_Artist")
public class ArtistNode implements Artist {

    @GraphId
    private Long id;
    @Indexed(unique = true)
    private String uuid;
    private String name;
    
    @RelatedTo(type = "RECORDED", direction = Direction.OUTGOING)
    private @Fetch Set<SongNode> recordedSongs;

    public ArtistNode() {
    }

    public ArtistNode(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void addRecorded(SongNode song) {
        if (recordedSongs == null) {
            recordedSongs = new HashSet<>();
        }
        recordedSongs.add(song);
    }

    public Set<SongNode> getRecordedSongs() {
        return recordedSongs;
    }
}
