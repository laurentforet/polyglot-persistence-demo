package coop.dev.polyglot.persistence.service;

import java.util.Set;
import org.springframework.stereotype.Service;

/**
 *
 * @author lforet
 */
@Service
public interface BasketService {

    void add(String userId, String itemId);
    void remove(String userId, String itemId);
    void clear(String userId);
    
    Set<String> getAll(String userId);
    
}
