package coop.dev.polyglot.persistence.impl.mongodb;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 *
 * @author lforet
 */
@EnableAutoConfiguration
@EnableMongoRepositories
@ComponentScan(basePackageClasses = MongoConfig.class)
public class MongoConfig {

//    @Bean
//    public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator() {
//        Resource sourceData = new ClassPathResource("song.json");
//        Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean();
//        factory.setMapper(new ObjectMapper());
//        factory.setResources(new Resource[]{sourceData});
//
//        return factory;
//    }
}
