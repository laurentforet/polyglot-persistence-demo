package coop.dev.polyglot.persistence.impl.neo4j;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author lforet
 */
public interface PersonRepository extends CrudRepository<PersonNode, String> {

    Iterable<PersonNode> findByLogin(String login);

    
}
