package coop.dev.polyglot.persistence.impl.neo4j;

import coop.dev.polyglot.persistence.domain.Song;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;

/**
 *
 * @author lforet
 */
@NodeEntity
@TypeAlias("_Song")
public class SongNode implements Song {
    
    @GraphId 
    private Long id;
    @Indexed(unique = true)
    private String uuid;
    private String title;

    public SongNode() {
    }

    public SongNode(String uuid, String title) {
        this.uuid = uuid;
        this.title = title;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    @Override
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
}
