package coop.dev.polyglot.persistence.impl.elasticsearch;

import java.io.IOException;
import java.util.Arrays;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component 
public class SearchInitializer {
    
    @Autowired
    private SongRepositoryES songRepository;
   
    
    public void init() {
        try {
            SongDocumentES[] songs = SONG_MAPPER.readValue(this.getClass().getClassLoader().getResourceAsStream(SONGS_JSON), SongDocumentES[].class);
            songRepository.save(Arrays.asList(songs));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        
    }
    
    private static final String SONGS_JSON = "songCollections.json";
    private static final ObjectMapper SONG_MAPPER = new ObjectMapper();
}
