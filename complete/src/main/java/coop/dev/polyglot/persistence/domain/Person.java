package coop.dev.polyglot.persistence.domain;

/**
 *
 * @author lforet
 */
public interface Person extends Identifiable {
    
    public String getLogin();
}
