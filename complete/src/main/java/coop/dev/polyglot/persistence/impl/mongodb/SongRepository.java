package coop.dev.polyglot.persistence.impl.mongodb;

import java.util.Collection;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * http://docs.mongodb.org/manual/tutorial/query-documents/
 * @author lforet
 */
public interface SongRepository extends MongoRepository<SongDocument, String>{
    
    
    public SongDocument findByTitle(String title);
    
    public List<SongDocument> findByUuidIn(Collection<String> uuids);
        
    public List<SongDocument> findByArtist(SongDocument.ArtistMongo artist);
    
    @Query("{ artist.uuid : ?0 }")
    public List<SongDocument> findByArtist(String uuid);
}
