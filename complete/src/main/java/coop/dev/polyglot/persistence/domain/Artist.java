package coop.dev.polyglot.persistence.domain;

/**
 *
 * @author lforet
 */
public interface Artist extends Identifiable {

    String getName();
}
