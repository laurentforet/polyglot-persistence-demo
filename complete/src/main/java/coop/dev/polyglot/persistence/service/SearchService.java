package coop.dev.polyglot.persistence.service;

import coop.dev.polyglot.persistence.domain.Song;

/**
 *
 * @author lforet
 */
public interface SearchService {

     Song findBy(String keyword);
    
}
