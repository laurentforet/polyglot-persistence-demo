package coop.dev.polyglot.persistence.impl.solr;

import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.repository.SolrCrudRepository;

/**
 *
 * @author lfo
 */
public interface SongRepository extends SolrCrudRepository<SongCollection, String>  {
    
    Page<SongCollection> findByLyricsContaining(Collection<String> lyrics, Pageable pageable);
    
}
