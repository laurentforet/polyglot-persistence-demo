package coop.dev.polyglot.persistence.impl.redis;

import coop.dev.polyglot.persistence.service.BasketService;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author lforet
 */
@Service
public class BasketServiceRedis implements BasketService {

    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    public BasketServiceRedis(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
        assert redisTemplate != null;
    }

    @Override
    public void add(String userId, String itemId) {
        redisTemplate.opsForSet().add(userId, itemId);
        redisTemplate.expire(userId, 7, TimeUnit.DAYS);
    }

    @Override
    public void remove(String userId, String itemId) {
        redisTemplate.opsForSet().remove(userId, itemId);
    }

    @Override
    public void clear(String userId) {
        redisTemplate.delete(userId);
    }

    @Override
    public Set<String> getAll(String userId) {
        return redisTemplate.opsForSet().members(userId);
    }

}
