package coop.dev.polyglot.persistence.domain;

import java.util.UUID;

/**
 *
 * @author lforet
 */
public interface Identifiable {

    String getUuid();
    
    public enum Generator {
        INSTANCE;
        
        public String generate() {
            return UUID.randomUUID().toString();
        }
    }
    
}
