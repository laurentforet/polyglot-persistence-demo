package coop.dev.polyglot.persistence.impl.neo4j;

import org.neo4j.graphdb.GraphDatabaseService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.rest.SpringRestGraphDatabase;

/**
 *
 * @author lforet
 */
@Configuration
@EnableNeo4jRepositories(basePackageClasses = Neo4jConfig.class)
@ComponentScan(basePackageClasses = Neo4jConfig.class)
public class Neo4jConfig extends Neo4jConfiguration{

    public Neo4jConfig() {
        setBasePackage("coop.dev.polyglot.persistence.impl");
    }
    
    @Bean
    GraphDatabaseService graphDatabaseService() {
//        return new GraphDatabaseFactory().newEmbeddedDatabase("target/test.db");
         return new SpringRestGraphDatabase("http://localhost:7474/db/data");
    }
    
}
