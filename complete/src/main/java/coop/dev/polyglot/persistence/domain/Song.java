package coop.dev.polyglot.persistence.domain;

/**
 *
 * @author lforet
 */
public interface Song  extends Identifiable {

    String getTitle();

}