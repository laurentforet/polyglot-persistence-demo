package coop.dev.polyglot.persistence.impl.neo4j;

import coop.dev.polyglot.persistence.domain.Person;
import java.util.HashSet;
import java.util.Set;
import org.neo4j.graphdb.Direction;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

/**
 *
 * @author lfo
 */
@NodeEntity
@TypeAlias("_Person")
public class PersonNode implements Person {
    
    @GraphId
    private Long id;
    @Indexed(unique = true)
    private String uuid;
    private String login;
    
    @RelatedTo(type = "LOVE", direction = Direction.OUTGOING)
    private @Fetch Set<SongNode> lovedSongs;

    public PersonNode() {
    }

    public PersonNode(String uuid, String login) {
        this.uuid = uuid;
        this.login = login;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    public void loves(SongNode song) {
        if (lovedSongs == null) {
            lovedSongs = new HashSet<>();
        }
        lovedSongs.add(song);
    }
    
    public Set<SongNode> getLovedSongs() {
        return lovedSongs;
    }
}
