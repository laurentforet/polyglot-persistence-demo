# README #

Ce projet contient le source de la présentation que j'aurai à donner au BordeauxJUG en septembre 2014 : la [persistance polyglotte](http://martinfowler.com/bliki/PolyglotPersistence.html).

 * lien avec les [microservices](http://martinfowler.com/articles/microservices.html).
 * mise en oeuvre de plusieurs moteurs de bases de données de différentes familles en fonction de leur spécificité :

    * Should have :
         * Stockage d'un catalogue : MongoDB 
         * Recommendation de chanson : Neo4j
         * Moteur de recherche : Solr, Elastic Search
         * Gestion d'un panier persistant : Redis  
   * Nice to have
         * Gestion des achats : RDBMS legacy database
         * Compteur de visiteurs : Cassandra


![nosql-space-from-neo4j](http://assets.neo4j.org/img/propertygraph/nosql-space.png)


# Use cases

  * Un utilisateur peut consulter un catalogue de chanson 
      * recherche sur un titre
      * recherche sur un auteur
      * recherche sur les paroles

    Le résultat contient le titre, l'auteur, et le nombre de ventes (Nice to have : map/reduce).

  * Un utilisateur peut aimer une chanson
  * Un utilisateur peut aimer, ne pas aimer un artiste (Nice to have)
  * Un utilisateur peut rajouter/supprimer dans son panier des chansons
  * Un utilisateur peut valider et acheter le contenu de son panier (Nice to have : legacy rdbms)
  * Un utilisateur peut obtenir des recommendations de chanson en fonction des goûts des autres utilisateurs.


# Le modèle 

  Artist -[:RECORD]-> Song

  Song -[:CONTAINS]-> Lyric
 
  User -[:BUY]-> Song

  User -[:LIKE]-> Song

# CAP theorem :

http://blog.nahurst.com/visual-guide-to-nosql-systems