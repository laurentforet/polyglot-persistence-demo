package dev.coop.polyglot.persistence.mongodb;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {MongoConfig.class})
public class CatalogInitializerTest {
    
    @Autowired
    private CatalogInitializer initializer;
    
    @Autowired
    private SongRepository repository;
    
    @Test
    public void testSimple() {
        repository.deleteAll();
        initializer.init();
        final List<SongDocument> suzaneSongs = repository.findByArtist("b262b540-bec6-4657-a074-99f7471693da");
         Assert.assertEquals(2, suzaneSongs.size());
    }

}
