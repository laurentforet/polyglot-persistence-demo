package dev.coop.polyglot.persistence.neo4j;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {Neo4jConfig.class})
public class RecommendationInitializerTest {

    @Autowired
    private RecommendationInitializer initializer;
    @Autowired
    private SongRepository songRepository;
    @Autowired
    private PersonRepository personRepository;
    
    @Test
    public void testSimple() {
        initializer.init();
        final PersonNode benoit = personRepository.findByLogin("Benoit P.");
        final List<SongNode> recommendedSongs = songRepository.findRecommendedSongs(benoit);
        Assert.assertEquals("Sober", recommendedSongs.get(0).getTitle());
        Assert.assertEquals("Mother", recommendedSongs.get(1).getTitle());
        Assert.assertEquals("Soul Catcher", recommendedSongs.get(2).getTitle());
        
    }

}
