package dev.coop.polyglot.persistence.elasticsearch;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {ElasticsearchConfig.class})
public class SearchInitializerTest {

    @Autowired
    private SearchInitializer searchInitializer;
    @Autowired
    private SongRepositoryES songRepository;

    @Test
    public void testSimple() {
        songRepository.deleteAll();
        searchInitializer.init();
        Assert.assertEquals(1, songRepository.findByLyricsContaining("Mother").size());
        final SongDocumentES searchByKeyword = songRepository.findByKeyword("mother").get(0);
        Assert.assertNotNull(searchByKeyword);

    }

}
