package dev.coop.polyglot.persistence.redis;

import dev.coop.polyglot.domain.Identifiable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {RedisConfig.class})
public class RedisBasketServiceTest {
    
    @Autowired
    private RedisBasketService service;
    
    @Test
    public void testSimple() {
        final String person1 = Identifiable.Generator.INSTANCE.create();
        final String object1 = Identifiable.Generator.INSTANCE.create();
        final String object2 = Identifiable.Generator.INSTANCE.create();
        final String object3 = Identifiable.Generator.INSTANCE.create();
        service.add(person1, object1);
        service.add(person1, object2);
        Assert.assertEquals(2, service.getBasket(person1).size());
        service.add(person1, object3);
        service.remove(person1, object2);
        Assert.assertEquals(2, service.getBasket(person1).size());
        service.clear(person1);
        Assert.assertEquals(0, service.getBasket(person1).size());
    }
    

}
