package dev.coop.polyglot.persistence.neo4j;

import dev.coop.polyglot.Const;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {Neo4jConfig.class})
public class ArtistRepositoryTest {

    @Autowired
    private GraphDatabaseService graphDatabaseService;
    
    @Autowired
    private ArtistRepository repository;

    @Test
    public void testSimple() {
        try (Transaction tx = graphDatabaseService.beginTx()) {
            repository.deleteAll();
            final ArtistNode suzane = new ArtistNode(Const.SUZANE_VEGA_ID, Const.SUZANE_VEGA);
            final SongNode soldierSong = new SongNode(Const.THE_SOLDIER_AND_THE_QUEEN_ID, Const.THE_SOLDIER_AND_THE_QUEEN);
            suzane.addRecordedSong(soldierSong);
            repository.save(suzane);
        }
    }

}
