package dev.coop.polyglot.persistence.mongodb;

import dev.coop.polyglot.Const;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {MongoConfig.class})
public class SongRepositoryTest {

    @Autowired
    private SongRepository songRepository;
    
    @Test
    public void testSimple() {
        songRepository.deleteAll();
        SongDocument.ArtistMongo suzaneVega = new SongDocument.ArtistMongo(Const.SUZANE_VEGA_ID, Const.SUZANE_VEGA);
        SongDocument soldierSong = new SongDocument(Const.THE_SOLDIER_AND_THE_QUEEN_ID, Const.THE_SOLDIER_AND_THE_QUEEN, suzaneVega);
        songRepository.save(soldierSong);
        SongDocument found = songRepository.findByTitle(Const.THE_SOLDIER_AND_THE_QUEEN).get(0);
        
        Assert.assertEquals(Const.SUZANE_VEGA_ID, found.getArtist().getId());
    }
    
}
