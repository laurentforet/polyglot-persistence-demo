package dev.coop.polyglot;

import dev.coop.polyglot.domain.Identifiable;

public class Const {

    //Persons
    public static final String LAURENT_ID = Identifiable.Generator.INSTANCE.create();
    public static final String LAURENT = "Laurent";
    
    // Artists
    public static final String SUZANE_VEGA_ID = Identifiable.Generator.INSTANCE.create();
    public static final String SUZANE_VEGA = "Suzane Vega";

    
    // Songs
    public static final String THE_SOLDIER_AND_THE_QUEEN_ID = Identifiable.Generator.INSTANCE.create();
    public static final String THE_SOLDIER_AND_THE_QUEEN = "The Soldier and The Queen";
}
