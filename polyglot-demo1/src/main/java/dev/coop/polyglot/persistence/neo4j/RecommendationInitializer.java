package dev.coop.polyglot.persistence.neo4j;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.stereotype.Component;

@Component
public class RecommendationInitializer {
@Autowired
    private GraphDatabaseService graphDatabaseService;

    @Autowired 
    private Neo4jTemplate template;
    
    @Autowired
    private ArtistRepository artistRepository;
    @Autowired
    private SongRepository songRepository;
    @Autowired
    private PersonRepository personRepository;

    public void init() {
        try (Transaction tx = graphDatabaseService.beginTx();) {
            template.query(DELETE_ALL, null);
            template.query(CREATE_ARTISTS
                    + CREATE_SONGS
                    + CREATE_PERSONS
                    + CREATE_LOVES, null);
            tx.success();
        }
    }

    private static final String DELETE_ALL =  "MATCH (a)\n"
            + "OPTIONAL MATCH (a)-[r]-()\n"
            + "DELETE a,r";

    private static final String CREATE_LOVES = "CREATE\n"
            + "       Me-[:LOVES]->Luka,\n"
            + "       Me-[:LOVES]->TomsDiner,\n"
            + "       Me-[:LOVES]->HighHopes,\n"
            + "       Me-[:LOVES]->Mother,\n"
            + "       Me-[:LOVES]->HeyYou,\n"
            + "       Me-[:LOVES]->SoulCatcher,\n"
            + "       Me-[:LOVES]->Sober,\n"
            + "       Me-[:LOVES]->SoWhat,\n"
            + "       Frederic-[:LOVES]->HighHopes,\n"
            + "       Frederic-[:LOVES]->Mother,\n"
            + "       Frederic-[:LOVES]->HeyYou,\n"
            + "       Fabrice-[:LOVES]->SoulCatcher,\n"
            + "       Lilian-[:LOVES]->Sober,\n"
            + "       Lilian-[:LOVES]->SoWhat,\n"
            + "       Benoit -[:LOVES]-> SoWhat,\n"
            + "       Laurent -[:LOVES]->Sober,\n"
            + "       Laurent -[:LOVES]-> SoulCatcher,\n"
            + "       Vincent -[:LOVES]->Sober,\n"
            + "       Vincent -[:LOVES]->Mother";
    private static final String CREATE_PERSONS = "CREATE (Me:_Person{login:'Laurent F.'}),\n"
            + "       (Frederic:_Person{login:'Frédéric C.'}),\n"
            + "       (Benoit:_Person{login:'Benoit P.'}),\n"
            + "       (Fabrice:_Person{login:'Fabrice A.'}),\n"
            + "       (Lilian:_Person{login:'Lilian B.'}),\n"
            + "       (Laurent:_Person{login:'Laurent B.'}),\n"
            + "       (Vincent:_Person{login:'Vincent Van S.'}),\n"
            + "       (Damien:_Person{login:'Damien R.'})";
    private static final String CREATE_SONGS = "CREATE SuzanneVega -[:RECORDED]-> (Luka:_Song{title:'Luka'}),\n"
            + "       SuzanneVega -[:RECORDED]-> (TomsDiner:_Song{title:'Tom\\'s Diner'}),\n"
            + "       PinkFloyd -[:RECORDED]-> (HighHopes:_Song{title:'High Hopes'}),\n"
            + "       PinkFloyd -[:RECORDED]-> (Mother:_Song{title:'Mother'}),\n"
            + "       PinkFloyd -[:RECORDED]-> (HeyYou:_Song{title:'HeyYou'}),\n"
            + "       TheThe -[:RECORDED]-> (SoulCatcher:_Song{title:'Soul Catcher'}),\n"
            + "       Pink -[:RECORDED]-> (Sober:_Song{title:'Sober'}),\n"
            + "       Pink -[:RECORDED]-> (SoWhat:_Song{title:'SoWhat'})";

    private static final String CREATE_ARTISTS = "CREATE (SuzanneVega:_Artist{name:'Suzanne Vega'}),\n"
            + "       (PinkFloyd:_Artist{name:'Pink Floyd'}),\n"
            + "       (TheThe:_Artist{name:'The The'}),\n"
            + "       (Pink:_Artist{name:'Pink'})";

}
