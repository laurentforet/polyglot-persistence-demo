package dev.coop.polyglot.persistence.neo4j;

import java.util.List;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.CrudRepository;

public interface SongRepository extends CrudRepository<SongNode, Long> {
    
    @Query("START person=node({0})\n"
            + "MATCH (person)-[:LOVES]->(song:_Song)<-[:LOVES]-(otherPerson:_Person)-[:LOVES]->(otherSong:_Song)<-[otherLoves:LOVES]-()\n"
            + "WITH otherSong as otherSong, count(otherLoves) AS counter\n"
            + "RETURN otherSong\n"
            + "ORDER BY counter DESC , otherSong.title\n"
            + "LIMIT 3"
    )
    List<SongNode> findRecommendedSongs(PersonNode person);
}
