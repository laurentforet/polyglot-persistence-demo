package dev.coop.polyglot.persistence.elasticsearch;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = ElasticsearchConfig.class)
public class ElasticsearchConfig {

}
