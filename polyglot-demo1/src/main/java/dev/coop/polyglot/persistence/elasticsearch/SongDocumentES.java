package dev.coop.polyglot.persistence.elasticsearch;

import dev.coop.polyglot.domain.Song;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;


@Document(indexName = "songindex", type = "song")
public class SongDocumentES implements Song {

    @Id
    private String uuid;
//    @Field(type = FieldType.String)
    private String title;
//    @Field(type = FieldType.String)
    private String lyrics;

    public SongDocumentES() {
    }

    public SongDocumentES(String uuid, String title, String lyrics) {
        this.uuid = uuid;
        this.title = title;
        this.lyrics = lyrics;
    }

    public String getId() {
        return uuid;
    }

    public void setId(String id) {
        this.uuid = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }
}
