package dev.coop.polyglot.persistence.neo4j;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface ArtistRepository extends CrudRepository<ArtistNode, Long> {
    
    public List<ArtistNode> findByName(String name);

}
