package dev.coop.polyglot.persistence.elasticsearch;

import java.util.List;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface SongRepositoryES extends ElasticsearchRepository<SongDocumentES, String> {

    public List<SongDocumentES> findByLyricsContaining(String keyword);
    
        @Query("{\n" +
"        \"bool\": {\n" +
"            \"should\": [\n" +
"                {\n" +
"                    \"term\": {\n" +
"                        \"title\": {\n" +
"                            \"value\": \"?0\",\n" +
"                            \"boost\": 3\n" +
"                        }\n" +
"                    }\n" +
"                },\n" +
"                {\n" +
"                    \"term\": {\n" +
"                        \"lyrics\": \"?0\"\n" +
"                    }\n" +
"                }\n" +
"            ]\n" +
"        }\n" +
"    }"
)
    List<SongDocumentES> findByKeyword(String keyword);
}
