package dev.coop.polyglot.persistence.mongodb;

import dev.coop.polyglot.domain.Artist;
import dev.coop.polyglot.domain.Song;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class SongDocument implements Song {
    
    @Id
    private String id;
    private String title;
    
    private ArtistMongo artist;

    public SongDocument() {
    }

    public SongDocument(String id, String title, ArtistMongo artist) {
        this.id = id;
        this.title = title;
        this.artist = artist;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArtistMongo getArtist() {
        return artist;
    }

    public void setArtist(ArtistMongo artist) {
        this.artist = artist;
    }
    
    public static class ArtistMongo implements Artist {
        
        private String id;
        private String name;

        public ArtistMongo() {
        }

        public ArtistMongo(String id, String name) {
            this.id = id;
            this.name = name;
        }

        @Override
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        
        
    }
}
