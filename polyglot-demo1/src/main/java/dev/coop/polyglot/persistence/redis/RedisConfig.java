package dev.coop.polyglot.persistence.redis;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;


@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {RedisConfig.class})
public class RedisConfig {
    
}
