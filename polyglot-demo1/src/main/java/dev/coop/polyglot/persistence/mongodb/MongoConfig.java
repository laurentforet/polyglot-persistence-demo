package dev.coop.polyglot.persistence.mongodb;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableAutoConfiguration
@Configuration
@EnableMongoRepositories
@ComponentScan(basePackageClasses = {MongoConfig.class})
public class MongoConfig {

}
