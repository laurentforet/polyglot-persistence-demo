package dev.coop.polyglot.persistence.redis;

import dev.coop.polyglot.service.BasketService;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisBasketService implements BasketService {

    @Autowired
    private RedisTemplate<String, String> template;
    
    public void add(String personId, String objectId) {
        template.opsForSet().add(personId, objectId);
        template.expire(personId, 7, TimeUnit.DAYS);
    }

    @Override
    public void remove(String personId, String objectId) {
        template.opsForSet().remove(personId, objectId);
    }

    @Override
    public void clear(String personId) {
        template.delete(personId);
    }

    @Override
    public Set<String> getBasket(String personId) {
        return template.opsForSet().members(personId);
    }
}
