package dev.coop.polyglot.persistence.neo4j;

import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<PersonNode, Long>{

    public PersonNode findByLogin(String login);
}
