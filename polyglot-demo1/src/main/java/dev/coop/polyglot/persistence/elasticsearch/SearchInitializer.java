package dev.coop.polyglot.persistence.elasticsearch;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SearchInitializer {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final ObjectReader reader = objectMapper.reader(SongDocumentES.class);;

    @Autowired
    private SongRepositoryES songRepository;

    public void init() {
        try {
            final MappingIterator<SongDocumentES> values = reader.readValues(this.getClass().getClassLoader().getResourceAsStream("songsElasticsearch.json"));
            songRepository.save(values.readAll());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
