package dev.coop.polyglot.persistence.mongodb;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CatalogInitializer {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final ObjectReader reader = objectMapper.reader(SongDocument.class);;

    @Autowired
    private SongRepository songRepository;

    public void init() {
        try {
            final MappingIterator<SongDocument> values = reader.readValues(this.getClass().getClassLoader().getResourceAsStream("songs.json"));
            songRepository.save(values.readAll());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
