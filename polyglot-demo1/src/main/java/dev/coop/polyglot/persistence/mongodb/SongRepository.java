package dev.coop.polyglot.persistence.mongodb;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;


public interface SongRepository extends MongoRepository<SongDocument, String>{

    List<SongDocument> findByTitle(String title);
    List<SongDocument> findByArtist(SongDocument.ArtistMongo artist);
    
    @Query("{ artist.id : ?0 }")
    List<SongDocument> findByArtist(String id);
}
