package dev.coop.polyglot.persistence.neo4j;

import dev.coop.polyglot.domain.Person;
import java.util.HashSet;
import java.util.Set;
import org.neo4j.graphdb.Direction;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
@TypeAlias("_Person")
public class PersonNode implements Person {

    @GraphId
    private Long id;
    @Indexed(unique = true)
    private String uuid;
    private String login;
    
    @RelatedTo(type = "LOVES", direction = Direction.OUTGOING)
    private Set<SongNode> lovedSongs;

    public PersonNode() {
    }

    public PersonNode(String uuid, String login) {
        this.uuid = uuid;
        this.login = login;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String getId() {
        return uuid;
    }

    public Set<SongNode> getLovedSongs() {
        return lovedSongs;
    }
    
    public void addLovedSongs(SongNode song) {
        if (lovedSongs == null) {
            lovedSongs = new HashSet<>();
        }
        lovedSongs.add(song);
    }

}
