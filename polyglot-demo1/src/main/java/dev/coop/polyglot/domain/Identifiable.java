package dev.coop.polyglot.domain;

import java.util.UUID;

public interface Identifiable {

    public String getId();

    public enum Generator {

        INSTANCE;

        public String create() {
            return UUID.randomUUID().toString();
        }
    }

}
