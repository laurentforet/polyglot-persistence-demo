package dev.coop.polyglot.domain;


public interface Person extends Identifiable {
     
    String getLogin();

}
