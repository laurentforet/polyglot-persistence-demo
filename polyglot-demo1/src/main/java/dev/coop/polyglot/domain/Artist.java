package dev.coop.polyglot.domain;


public interface Artist extends Identifiable {

    String getName();
    
}
