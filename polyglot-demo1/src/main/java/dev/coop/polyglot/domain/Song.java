package dev.coop.polyglot.domain;


public interface Song extends Identifiable {

    String getTitle();
    
}
