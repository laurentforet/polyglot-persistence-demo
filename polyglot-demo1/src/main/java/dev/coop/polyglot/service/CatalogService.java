package dev.coop.polyglot.service;

import dev.coop.polyglot.domain.Artist;
import dev.coop.polyglot.domain.Song;
import java.util.Collection;


public interface CatalogService {

    Collection<Song> getAllSongs();
    Collection<Song> getSongsOf(Artist artist);
}
