package dev.coop.polyglot.service;

import java.util.Set;


public interface BasketService {

    void add(String personId, String objectId);
    void remove(String personId, String objectId);
    void clear(String personId);
    
    Set<String> getBasket(String personId);
    
}
