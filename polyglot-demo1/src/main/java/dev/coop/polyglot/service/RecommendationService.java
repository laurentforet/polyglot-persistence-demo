package dev.coop.polyglot.service;

import dev.coop.polyglot.domain.Person;
import dev.coop.polyglot.domain.Song;
import java.util.Collection;


public interface RecommendationService {

    Collection<Song> getRecommendedSongs(Person person);
}
