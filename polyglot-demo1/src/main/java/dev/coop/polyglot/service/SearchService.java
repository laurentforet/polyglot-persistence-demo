package dev.coop.polyglot.service;

import dev.coop.polyglot.domain.Song;
import java.util.Collection;


public interface SearchService {

    Collection<Song> findByKeywords(String... keyword);
    
}
